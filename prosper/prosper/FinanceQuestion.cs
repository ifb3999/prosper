﻿using System;
using System.Collections.Generic;
using System.Text;

namespace prosper
{
    public class FinanceQuestion
    {
        public string Question { get; private set; }

        public string Explanation { get; private set; }
        public string CorrectAnswer { get; private set; }
        public string[] Answers { get; private set; }

        public FinanceQuestion(string question, string[] answers, string explanation)
        {
            Question = question;
            Answers = answers;
            CorrectAnswer = answers[0];
            Explanation = explanation;
        }

        public bool IsAnswerCorrect(string providedAnswer)
        {
            if (providedAnswer == CorrectAnswer)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public string[] JumbledAnswers()
        {
            string[] jumbledAnswers = this.Answers;
            // Knuth shuffle algorithm :: courtesy of Wikipedia :)
            System.Random random = new System.Random();
            for (int i = 0; i < jumbledAnswers.Length; i++)
            {
                int j = random.Next(i, jumbledAnswers.Length); // Don't select from the entire array on subsequent loops
                string temp = jumbledAnswers[i];
                jumbledAnswers[i] = jumbledAnswers[j];
                jumbledAnswers[j] = temp;
            }
            return jumbledAnswers;
        }
    }
}
