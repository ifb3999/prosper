﻿using System;
using System.Collections.Generic;
using System.Text;

namespace prosper
{
    public class Transaction
    {
        public Transaction(int amount, string description)
        {
            Amount = amount;
            Description = description;
        }
        public string Description { get; set; }
        public int Amount { get; set; }
    }
}
