﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using System.Windows.Input;
using Xamarin.Forms;
using System.Collections.ObjectModel;

namespace prosper
{
    class ManageMoneyViewModel : INotifyPropertyChanged
    {
        public Game game = Game.Instance;
        public event PropertyChangedEventHandler PropertyChanged;  
        string goalImageSource;
        string goalStageTitle;
        string goalProductTitle;
        string goalProductExplanation;


        public string GoalImageSource
        {
            get
            {
                return game.GoalImageSource;
            }
            set
            {
                if (goalImageSource != value)
                {
                    goalImageSource = value;
                    OnPropertyChanged(nameof(GoalImageSource));
                }
                
            }
        }
        public string GoalStageTitle
        {
            get
            {
                return game.GoalStageTitle;
            }
            set
            {
                goalStageTitle = value;
                OnPropertyChanged(nameof(GoalStageTitle));
            }
        }
        public string GoalProductTitle
        {
            get
            {
                return game.GoalProductTitle;
            }
            set
            {
                goalProductTitle = value;
                OnPropertyChanged(nameof(GoalProductTitle));
            }
        }
        public string GoalProductExplanation
        {
            get
            {
                return game.GoalProductExplanation;
            }
            set
            {
                goalProductExplanation = value;
                OnPropertyChanged(nameof(GoalProductExplanation));
            }
        }
        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
