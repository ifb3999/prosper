﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace prosper
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class EarnMoney : ContentPage
	{
		public EarnMoney ()
		{
			InitializeComponent ();
		}
        async void OnMowingSelect()
        {
            //DisplayAlert("Mow The Lawn", "... awaiting implementation", "ok");
            //Game.Instance.MiniStartTimer();
            await Navigation.PushAsync(new MowGame());
        }
        async void OnCleanSelect()
        {
            await Navigation.PushAsync(new CleanTheHouse());
        }
        async void OnTutorSelect()
        {
            await Navigation.PushAsync(new FinanceTutor());
        }
        async void OnCashierSelect()
        {
            await Navigation.PushAsync(new CashierGame());
        }

        async void OnRoomSelection(object sender, EventArgs args)
        {
            Button button = (Button)sender;
            //set character to char 1
            //Game.Instance.Character = 1;
            //open my room
            Page currentPage = App.Current.MainPage.Navigation.NavigationStack.LastOrDefault();
            Application.Current.MainPage.Navigation.InsertPageBefore(new MyRoom(), currentPage);
            await Application.Current.MainPage.Navigation.PopAsync();
        }

        async void OnAchievementSelection(object sender, EventArgs args)
        {
            Button button = (Button)sender;
            Page currentPage = App.Current.MainPage.Navigation.NavigationStack.LastOrDefault();
            Application.Current.MainPage.Navigation.InsertPageBefore(new Achievement(), currentPage);
            await Application.Current.MainPage.Navigation.PopAsync();
        }

        async void OnPlayTutorial(object sender, EventArgs args)
        {
            //when finished tutorial
            //game.GameInitialised = true;
            //TutorialPopupVisible = false;
            //temporary
            Application.Current.MainPage.DisplayAlert("Tutorial", "...awaiting implementation", "OK");
            await Application.Current.MainPage.Navigation.PopAsync();

        }

        async void OnHelpSelection(object sender, EventArgs args)
        {
            Button button = (Button)sender;
            Page currentPage = App.Current.MainPage.Navigation.NavigationStack.LastOrDefault();
            Application.Current.MainPage.Navigation.InsertPageBefore(new Help(), currentPage);
            await Application.Current.MainPage.Navigation.PopAsync();
        }
    }
}