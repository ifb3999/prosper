﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace prosper
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Shop : ContentPage
	{
		public Shop ()
		{
			InitializeComponent ();
		}

        async void OnRoomSelection(object sender, EventArgs args)
        {
            Button button = (Button)sender;
            //set character to char 1
            //Game.Instance.Character = 1;
            //open my room
            Page currentPage = App.Current.MainPage.Navigation.NavigationStack.LastOrDefault();
            Application.Current.MainPage.Navigation.InsertPageBefore(new MyRoom(), currentPage);
            await Application.Current.MainPage.Navigation.PopAsync();
        }

        async void OnGoodsBoughtSelection(object sender, EventArgs args)
        {
            Game.Instance.BuyNewGoods = true;
            Game.Instance.MoneyTotal -= Game.Instance.Prices[Game.Instance.Goods][0];
            Game.Instance.GameTransactions.Add(new Transaction((Game.Instance.Prices[Game.Instance.Goods][0])*-1, "Shop Purchase"));
            if ((Game.Instance.Happiness + Game.Instance.Prices[Game.Instance.Goods][1]) <= 100)
            {
                Game.Instance.Happiness += Game.Instance.Prices[Game.Instance.Goods][1];
            }
            else if ((Game.Instance.Happiness + Game.Instance.Prices[Game.Instance.Goods][1]) > 100)
            {
                Game.Instance.Happiness = 100;
            }
            Button button = (Button)sender;
            Page currentPage = App.Current.MainPage.Navigation.NavigationStack.LastOrDefault();
            Application.Current.MainPage.Navigation.InsertPageBefore(new MyRoom(), currentPage);
            await Application.Current.MainPage.Navigation.PopAsync();
        }

        async void OnAchievementSelection(object sender, EventArgs args)
        {
            Button button = (Button)sender;
            Page currentPage = App.Current.MainPage.Navigation.NavigationStack.LastOrDefault();
            Application.Current.MainPage.Navigation.InsertPageBefore(new Achievement(), currentPage);
            await Application.Current.MainPage.Navigation.PopAsync();
        }

        async void OnHelpSelection(object sender, EventArgs args)
        {
            Button button = (Button)sender;
            Page currentPage = App.Current.MainPage.Navigation.NavigationStack.LastOrDefault();
            Application.Current.MainPage.Navigation.InsertPageBefore(new Help(), currentPage);
            await Application.Current.MainPage.Navigation.PopAsync();
        }

        async void OnPlayTutorial(object sender, EventArgs args)
        {
            //when finished tutorial
            //game.GameInitialised = true;
            //TutorialPopupVisible = false;
            //temporary
            Application.Current.MainPage.DisplayAlert("Tutorial", "...awaiting implementation", "OK");
            await Application.Current.MainPage.Navigation.PopAsync();

        }

    }
}