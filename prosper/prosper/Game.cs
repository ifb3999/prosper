﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Xamarin.Forms;

namespace prosper
{
    public sealed class Game
    {
        //implement static initilisation Singleton pattern - Reference, https://msdn.microsoft.com/en-au/library/ff650316.aspx
        private static readonly Game instance = new Game();
        public const int Minutes = 1;
        public const int Seconds = 30;
        public bool hitRock = false;
        public bool mowGameFinished = false;

        public static Game Instance
        {
            get
            {
                return instance;
            }
        }
        private Game()
        {
            Character = 1;
            GameInitialised = false;
            StageInitialised = false;
            GameStage = Stage.One;
            //start with no money
            MoneyTotal = 0;
            MoneyGoal = 500;;
            //start with full happiness
            Happiness = 100;
            Goods = "none";
            BuyNewGoods = false;

            Prices.Add("apple", new int[]{ 3, 3,0,6 });
            Prices.Add("banana", new int[] { 10, 6,0,48 });
            Prices.Add("orange", new int[] { 7, 5,0,18 });
            Prices.Add("camera", new int[] { 100, 21,3,48 });
            Prices.Add("kindle", new int[] { 80, 16,3,30 });
            Prices.Add("book", new int[] { 12, 7,0,18 });
            GoodsNumber = -1;
            GoodsCounter = new int[9];
            GoodsMins = new int[9];
            for (int i = 0;i<9;i++)
            {
                GoodsCounter[i] = 0;
                GoodsMins[i] = 0;
            }

            GoodsImageSource = new string[9];
            for (int i = 0; i < 9; i++)
            {
                GoodsImageSource[i] = "none.png";
            }
            GoodsVisible = new bool[9];
            for (int i = 0; i < 9; i++)
            {
                GoodsVisible[i] = false;
            }
        }
        public ObservableCollection<Transaction> GameTransactions = new ObservableCollection<Transaction>();
        public bool IsTimerCancel { get; set; } = false;
        public int Character { get; set; }
        public bool GameInitialised { get; set; }
        public bool StageInitialised { get; set; }
        public bool NotificationActive { get; set; } = false;
        public string Goods { get; set; }
        public bool BuyNewGoods { get; set; }

        public TimeSpan timerInterval;
        public double MoneyTotal { get; set; }
        public int BillsAmount { get; set; } = 0;

        public Dictionary<string, int[]> Prices = new Dictionary<string, int[]>();
        public int[] GoodsCounter { get; set; }
        public int[] GoodsMins { get; set; }
        public int GoodsNumber { get; set; }
        public string[] GoodsImageSource { get; set; }
        public bool[] GoodsVisible { get; set; }

        public string GoalImageSource
        {
            get
            {
                if(this.GameStage == Stage.Three)
                {
                    return "stage3travel.png";
                }
                else if(this.GameStage == Stage.Two)
                {
                    return "stage2car.png";
                }
                else
                {
                    return "stage1phone.png";
                }
            }
        }
        public string GoalStageTitle
        {
            get
            {
                if (this.GameStage == Stage.Three)
                {
                    return "Current Goal - Stage 3";
                }
                else if (this.GameStage == Stage.Two)
                {
                    return "Current Goal - Stage 2";
                }
                else
                {
                    return "Current Goal - Stage 1";
                }
            }
        }
        public string GoalProductTitle
        {
            get
            {
                if (this.GameStage == Stage.Three)
                {
                    return "World Trip";
                }
                else if (this.GameStage == Stage.Two)
                {
                    return "New Car";
                }
                else
                {
                    return "Mobile Phone";
                }
            }
        }
        public string GoalProductExplanation
        {
            get
            {
                if (this.GameStage == Stage.Three)
                {
                    return "You are saving for a holiday around the world. You need to save $10000 to cover your accommodation, flights, and tour activities";
                }
                else if (this.GameStage == Stage.Two)
                {
                    return "You are saving for a brand new car so that you can drive yourself places. You need to save $5000 to purchase the car. " +
                        "\n *Remember you will have ongoing costs: monthly insurance and registration costs ";
                }
                else
                {
                    return "You are saving for a mobile phone so you can contact your friends and play games. You need to save $500 to purchase the phone." +
                        "\n *Remember you will have ongoing costs: monthly phone credit";
                }
            }
        }

        public double MoneyProgress
        {
            get
            {
                return this.MoneyTotal / this.MoneyGoal;
            }
        }

        public double HappinessProgress
        {
            get
            {
                return this.Happiness / 100;
            }
        }

        public double MoneyGoal { get; set; } = 500;
        public double Happiness { get; set; }
        public string TimerText { get; private set; }
        public string EarnedMoney { get; set; }

        public enum Stage
        {
            One,
            Two,
            Three
        };
        public Stage GameStage { get; set; }
        public enum Status
        {
            GameLost,
            GameWon,
            Stage1Active,
            Stage1Completed,
            Stage2Active,
            Stage2Completed,
            Stage3Active,
            Restart,
        };
        public Status GameStatus { get; set; }

        public void CheckGameStatus()
        {
            if (MoneyTotal<0 || Happiness <= 0)
            {
                NotificationActive = true;
                GameStatus = Status.GameLost;
            }
            if (MoneyTotal >= MoneyGoal)
            {
                //stage has been completed
                IsTimerCancel = true;
                NotificationActive = true;

                if (GameStage == Stage.One)
                {
                    GameStatus = Status.Stage1Completed;
                    //progress to stage two
                    MoneyGoal = 5000;
                    GameStage = Stage.Two;
                    BillsAmount = 10;
                }
                else if(GameStage == Stage.Two)
                {
                    GameStatus = Status.Stage2Completed;
                    //progress to stage three
                    MoneyGoal = 15000;
                    GameStage = Stage.Three;
                    BillsAmount = 20;
                }
                else if(GameStage == Stage.Three)
                {

                    GameStatus = Status.GameWon;
                    //game is completed
                }
            }
        }

        public void RestartGame()
        {
            MoneyGoal = 500;
            MoneyTotal = 0;
            Happiness = 1;
            GameStage = Stage.One;
            Character = 1;
            GameInitialised = false;
            StageInitialised = false;
            IsTimerCancel = false;
            Goods = "none";
            BuyNewGoods = false;
        }

        public void StartTimer()
        {
            int mins = Minutes;
            int counter = Seconds;
            Device.StartTimer(new TimeSpan(0, 0, 1), () =>
            {
                if (IsTimerCancel)
                {
                    return false;
                }
                else
                {
                    Device.BeginInvokeOnMainThread(() =>
                    {
                        counter = counter - 1;
                        if (counter < 0)
                        {
                            counter = 59;
                            mins = mins - 1;
                            if (mins < 0)
                            {
                                    mins = 0;
                                    counter = 0;
                            }
                        }

                        TimerText = string.Format("{0:00}:{1:00}", mins, counter);
                    });
                    if ( mins == 0 && counter == 0)
                    {
                        //what to do when the timer finishes
                        Happiness -= 0.1;
                        //will need to deduct bills depeneding on stage
                        MoneyTotal -= BillsAmount;
                        GameTransactions.Add(new Transaction(BillsAmount*-1, "Bills"));
                        //restart the timer again
                        StartTimer();
                        return false;
                    }
                    else
                    {
                        return true;
                    }
                }
            });

        }
     
    }
}