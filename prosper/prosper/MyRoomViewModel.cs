﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;


namespace prosper
{
    class MyRoomViewModel : INotifyPropertyChanged
    {
        bool sideBarVisible = false;
        bool randomPopupVisible = false;
        bool tutorialPopupVisible = false;
        bool stagePopupVisible = false;
        bool characterVisible = false;
        //bool[] goodsVisible;
        bool congratulationsVisible = false;
        string randomPopupText;
        string randomPopupMoneyAmount;
        string randomPopupHappinessAmount;
        string stagePopupTitle;
        string stagePopupText;
        string stageBeginText;
        string characterImageSource;
        //string[] goodsImageSource;
        string congratulationsText;
        public Game game = Game.Instance;
        public event PropertyChangedEventHandler PropertyChanged;
        public ICommand PlayTutorialCommand { get; private set; }
        public ICommand SkipTutorialCommand { get; private set; }
        public ICommand BeginStageCommand { get; private set; }
        public ICommand RandomEventCommand { get; private set; }
        public ICommand CloseRandomEventCommand { get; private set; }
        public ICommand SideBarCommand { get; private set; }
        public ICommand SideBarDisappear { get; private set; }
        public ICommand ContinueCommand { get; private set; }


        public bool SideBarVisible
        {
            get
            {
                return sideBarVisible;
            }
            set
            {
                sideBarVisible = value;
                OnPropertyChanged(nameof(SideBarVisible));
            }
        }

        public MyRoomViewModel()
        {
            SideBarCommand = new Command(OnSideBarSelect);
            SideBarDisappear = new Command(OnSideBarNotSelect);
            PlayTutorialCommand = new Command(OnPlayTutorial);
            SkipTutorialCommand = new Command(OnSkipTutorial);
            BeginStageCommand = new Command(OnStageBegin);
            RandomEventCommand = new Command(OnRandomSelect);
            CloseRandomEventCommand = new Command(CloseRandomEvent);
            ContinueCommand = new Command(CloseCongratulations);

            //goodsImageSource = new string[9];
            //for (int i = 0; i < 9; i++)
            //{
            //    goodsImageSource[i] = "none.png";
            //}
            //goodsVisible = new bool[9];
            //for (int i = 0; i < 9; i++)
            //{
            //    goodsVisible[i] = false;
            //}

            AddCharacterToScreen(game.Character);
            AddGoodsToScreen(game.Goods);

            if (!game.GameInitialised)
            {
                TutorialPopupVisible = true;
            }
            else if (!game.StageInitialised)
            {
                // if not initialised 
                // will see if this is a use case
                Application.Current.MainPage.DisplayAlert("Code Check", "Nothing for loading page with game intialised but stage not initialised", "Alright");
            }


        }

        void OnSideBarSelect()
        {

            SideBarVisible = true;
        }
        void OnSideBarNotSelect()
        {

            SideBarVisible = false;
        }

        void OnRandomSelect()
        {

            //display a content view
            RandomEvent randomEvent = new RandomEvent();
            //adjust happiness
            if (randomEvent.HappinessOutcome == "win")
            {
                game.Happiness += randomEvent.HappinessAmount;
            }
            else if (randomEvent.HappinessOutcome == "lose")
            {
                game.Happiness -= randomEvent.HappinessAmount;
            }
            else
            {
                //TODO raise an exception
            }
            //adjust money
            if (randomEvent.MoneyOutcome == "win")
            {
                game.MoneyTotal += randomEvent.MoneyAmount;
                game.GameTransactions.Add(new Transaction((int)randomEvent.MoneyAmount, "Random Event"));
            }
            else if (randomEvent.MoneyOutcome == "lose")
            {
                game.MoneyTotal -= randomEvent.MoneyAmount;
                game.GameTransactions.Add(new Transaction((int)randomEvent.MoneyAmount*-1, "Random Event"));
            }
            else
            {
                //TODO raise an exception
            }

            if (randomEvent.MoneyOutcome == "win")
            {
                RandomPopupMoneyAmount = "$" + randomEvent.MoneyAmount.ToString();
            }
            else if (randomEvent.MoneyOutcome == "lose")
            {
                RandomPopupMoneyAmount = "-$" + randomEvent.MoneyAmount.ToString();
            }


            if (randomEvent.HappinessOutcome == "win")
            {
                RandomPopupHappinessAmount = randomEvent.HappinessAmount.ToString("P0");
            }
            else if (randomEvent.HappinessOutcome == "lose")
            {
                RandomPopupHappinessAmount = "-" + randomEvent.HappinessAmount.ToString("P0");
            }


            RandomPopupText = randomEvent.EventText;
            RandomPopupVisible = true;

            //Application.Current.MainPage.DisplayAlert("Random Event", "You earnt $100", "ok");
        }
        void OnPlayTutorial()
        {
            //when finished tutorial
            game.GameInitialised = true;
            TutorialPopupVisible = false;
            //temporary
            Application.Current.MainPage.DisplayAlert("Tutorial", "...awaiting implementation", "OK");
            if (!game.StageInitialised)
            {
                InitialiseStage();
            }
        }
        void OnSkipTutorial()
        {
            game.GameInitialised = true;
            TutorialPopupVisible = false;
            if (!game.StageInitialised)
            {
                InitialiseStage();
            }
        }

        void CloseRandomEvent()
        {
            //set random event popup invisible
            RandomPopupVisible = false;
        }
        void OnStageBegin()
        {
            game.StartTimer();
            game.StageInitialised = true;
            StagePopupVisible = false;

        }

        void CloseCongratulations()
        {
            CongratulationsVisible = false;
            //GoodsVisible = false;
            Game.Instance.BuyNewGoods = false;
        }

        void InitialiseStage()
        {

            if (game.GameStage == Game.Stage.One)
            {
                StagePopupTitle = "Stage 1";
                StagePopupText = "The objective of stage 1 is to save $100 to purchase a mobile phone." +
                    "\nYou can earn money from doing chores for pocket money." +
                    "\nTo avoid running low on happiness you can buy items from the shop.";
                StageBeginText = "Begin Stage 1!";
            }
            else if (game.GameStage == Game.Stage.Two)
            {
                StagePopupTitle = "Stage 2";
                StagePopupText = "The objective of stage 2 is to save $1000 to purchase a car." +
                    "\nYou can now earn money faster from working different jobs." +
                    "\nYou will now pay extra bills per round for your phone";
                StageBeginText = "Begin Stage 2!";
            }
            else if (game.GameStage == Game.Stage.Three)
            {
                StagePopupTitle = "Stage 3";
                StagePopupText = "The objective of stage 3 is to save $5000 to purchase a holiday." +
                    "\nYou can now also earn money through investing - see Manage $." +
                    "\nYou will now pay extra bills per round for your car.";
                StageBeginText = "Begin Stage 3!";
            }
            StagePopupVisible = true;

        }

        void AddCharacterToScreen(int characterNum)
        {
            switch (characterNum)
            {
                case 1:
                    CharacterImageSource = "char1.png";
                    break;
                case 2:
                    CharacterImageSource = "char2.png";
                    break;
                case 3:
                    CharacterImageSource = "char3.png";
                    break;
                default:
                    //should throw an error I think
                    break;
            }
            CharacterVisible = true;
        }

        void AddGoodsToScreen(string goodsName)
        {

            if (goodsName != "none")
            {
                //goodsImageSource = string.Format("{0}{1}", goodsName, "3.png");
                GoodsImageSource[game.GoodsNumber] = string.Format("{0}{1}", goodsName, "3.png");
                if (game.BuyNewGoods == true)
                {
                    CongratulationsVisible = true;
                    if (goodsName.StartsWith("a") || goodsName.StartsWith("e") || goodsName.StartsWith("i") || goodsName.StartsWith("o") || goodsName.StartsWith("u"))
                    {
                        CongratulationsText = string.Format("{0}{1}{2}", "You buy an ", goodsName, " successfully.");
                    }
                    else
                    {
                        CongratulationsText = string.Format("{0}{1}{2}", "You buy a ", goodsName, " successfully.");
                    }
                }

                GoodsVisible[game.GoodsNumber] = true;


                for (int i = 0; i < (game.GoodsNumber+1); i++)
                {
                    int temp = i;
                    //Console.WriteLine("i: " + temp);
                    Device.StartTimer(new TimeSpan(0, 0, 1), () =>
                    {
                        //Device.BeginInvokeOnMainThread(() =>
                        //{
                        game.GoodsCounter[temp] = game.GoodsCounter[temp] - 1;
 
                        if (game.GoodsCounter[temp] < 0)
                        {
                            game.GoodsCounter[temp] = 59;
                            game.GoodsMins[temp] = game.GoodsMins[temp] - 1;
                            if (game.GoodsMins[temp] < 0)
                            {
                                game.GoodsMins[temp] = 0;
                                game.GoodsCounter[temp] = 0;
                            }
                        }

                        Console.WriteLine(temp + ": " + game.GoodsCounter[temp]);
                        //});
                        if (game.GoodsMins[temp] == 0 && game.GoodsCounter[temp] == 0)
                        {
                            GoodsVisible[temp] = false;
                            OnPropertyChanged(nameof(GoodsVisible));
                            //Console.WriteLine(temp + ": false " );
                            return false;
                        }
                        else
                        {
                            return true;
                        }

                    });
                }

            }
        }

        public string CongratulationsText
        {
            get
            {
                return congratulationsText;
            }
            set
            {
                congratulationsText = value;
                OnPropertyChanged("CongratulationsText");
            }
        }

        public bool CongratulationsVisible
        {
            get
            {
                return congratulationsVisible;
            }
            set
            {
                congratulationsVisible = value;
                OnPropertyChanged(nameof(CongratulationsVisible));
            }
        }

        public bool RandomPopupVisible
        {
            get
            {
                return randomPopupVisible;
            }
            set
            {
                randomPopupVisible = value;
                OnPropertyChanged(nameof(RandomPopupVisible));
            }
        }

        public string RandomPopupHappinessAmount
        {
            get
            {
                return randomPopupHappinessAmount;
            }
            set
            {
                randomPopupHappinessAmount = value;
                OnPropertyChanged(nameof(RandomPopupHappinessAmount));
            }
        }
        public string RandomPopupMoneyAmount
        {
            get
            {
                return randomPopupMoneyAmount;
            }
            set
            {
                randomPopupMoneyAmount = value;
                OnPropertyChanged(nameof(RandomPopupMoneyAmount));
            }
        }

        public string RandomPopupText
        {
            get
            {
                return randomPopupText;
            }
            set
            {
                randomPopupText = value;
                OnPropertyChanged(nameof(RandomPopupText));
            }
        }

        public bool TutorialPopupVisible
        {
            get
            {
                return tutorialPopupVisible;
            }
            set
            {
                tutorialPopupVisible = value;
                OnPropertyChanged(nameof(TutorialPopupVisible));
            }
        }
        public bool CharacterVisible
        {
            get
            {
                return characterVisible;
            }
            set
            {
                characterVisible = value;
                OnPropertyChanged(nameof(CharacterVisible));
            }
        }

        public bool[] GoodsVisible
        {
            get
            {
                return game.GoodsVisible;
            }
            set
            {
                game.GoodsVisible = value;
                OnPropertyChanged(nameof(GoodsVisible));
            }
        }

        public bool StagePopupVisible
        {
            get
            {
                return stagePopupVisible;
            }
            set
            {
                stagePopupVisible = value;
                OnPropertyChanged("StagePopupVisible");
            }
        }
        public string StagePopupTitle
        {
            get
            {
                return stagePopupTitle;
            }
            set
            {
                stagePopupTitle = value;
                OnPropertyChanged("StagePopupTitle");
            }
        }
        public string StagePopupText
        {
            get
            {
                return stagePopupText;
            }
            set
            {
                stagePopupText = value;
                OnPropertyChanged("StagePopupText");
            }
        }
        public string StageBeginText
        {
            get
            {
                return stageBeginText;
            }
            set
            {
                stageBeginText = value;
                OnPropertyChanged("StageBeginText");
            }
        }
        public string CharacterImageSource
        {
            get
            {
                return characterImageSource;
            }
            set
            {
                characterImageSource = value;
                OnPropertyChanged("CharacterImageSource");
            }
        }

        public string[] GoodsImageSource
        {
            get
            {
                return game.GoodsImageSource;
            }
            set
            {
                game.GoodsImageSource = value;
                OnPropertyChanged("GoodsImageSource");
            }
        }



        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
