﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using Xamarin.Forms;
using System.Windows.Input;
using System.Linq;

namespace prosper
{
    public class GameStatsViewModel : INotifyPropertyChanged
    {
        public Game game = Game.Instance;
        bool notificationIsVisible = false;
        bool jobGamesUnlocked = true;
        string notificationPopupTitle;
        string notificationPopupText;
        string notificationButtonText;
        public ICommand NotificationButtonCommand { get; private set; }
        string roundTimer;        
        public event PropertyChangedEventHandler PropertyChanged;

        public GameStatsViewModel()
        {
            NotificationIsVisible = false;
            NotificationButtonCommand = new Command(OnNotificationButtonAsync);
            Device.StartTimer(TimeSpan.FromSeconds(1), () =>
            {
                OnPropertyChanged(nameof(RoundTimer));
                OnPropertyChanged(nameof(JobGamesUnlocked));
                OnPropertyChanged(nameof(Happiness));
                OnPropertyChanged(nameof(MoneyProgress));
                OnPropertyChanged(nameof(HappinessProgress));
                OnPropertyChanged(nameof(MoneyTotal));
                OnPropertyChanged(nameof(MoneyGoal));
                game.CheckGameStatus();
                if (game.NotificationActive)
                {
                    RenderNotification();
                }
                return true;
            });
        }

        void RenderNotification()
        {
            //for end of game
            if (game.GameStatus == Game.Status.GameLost)
            {
                NotificationPopupTitle = "Game Over";
                NotificationPopupText = "You ran out of happinesss or money.\n" +
                    " You can restart the game to play again.";
                NotificationButtonText = "Restart Game";

                
            }
            else if (game.GameStatus == Game.Status.Stage1Completed)
            {
                NotificationPopupTitle = "Begin Stage 2";
                NotificationPopupText = "Congratulations you've completed stage ! You now have a mobile phone and have to pay $10 bills per round." +
                    "\nIn stage 2 you have unlocked new items in the shop and you can play job games to earn more money to save towards a car.";
                NotificationButtonText = "Play";
            }
            else if (game.GameStatus == Game.Status.Stage2Completed)
            {
                NotificationPopupTitle = "Begin Stage 3";
                NotificationPopupText = "Congratulations you've completed stage 2! You now have a car and have to pay an extra $50 bills per round.\n" +
                    "In stage 3 you have unlocked new items in the shop and you can invest your money to grow your wealth to buy a holiday.";
                NotificationButtonText = "Play";
            }
            else if (game.GameStatus == Game.Status.GameWon)
            {
                NotificationPopupTitle = "Game Completed";
                NotificationPopupText = "Congratulations you've completed the game! You have learnt how to earn and manage your money." +
                    "If you would like you can play again.";
                NotificationButtonText = "Play Again";
            }
            else
            {
                //TODO catch the exception
            }
            NotificationIsVisible = true;
        }

        public bool NotificationIsVisible
        {
            set
            {
                if (notificationIsVisible != value)
                {
                    notificationIsVisible = value;
                    OnPropertyChanged(nameof(NotificationIsVisible));
                }
            }
            get
            {
                return notificationIsVisible;
            }
        }



        public string NotificationPopupTitle
        {
            set
            {
                if(notificationPopupTitle != value)
                {
                    notificationPopupTitle = value;
                    OnPropertyChanged(nameof(NotificationPopupTitle));
                }
            }
            get
            {
                return notificationPopupTitle;
            }
        }
        public string NotificationPopupText
        {
            set
            {
                if (notificationPopupText != value)
                {
                    notificationPopupText = value;
                    OnPropertyChanged(nameof(NotificationPopupText));
                }
            }
            get
            {
                return notificationPopupText;
            }
        }
        public string NotificationButtonText
        {
            set
            {
                if (notificationButtonText != value)
                {
                    notificationButtonText = value;
                    OnPropertyChanged(nameof(NotificationButtonText));
                }
            }
            get
            {
                return notificationButtonText;
            }
        }

        async void OnNotificationButtonAsync()
        {
            if (NotificationButtonText == "Restart Game" || NotificationButtonText == "Play Again")
            {
               
                game.GameStatus = Game.Status.Restart;
                //set up the game from beginning
                game.RestartGame();
                //Application.Current.MainPage = new SelectCharacter();
                //await Application.Current.MainPage.Navigation.PopToRootAsync();
                //await Application.Current.MainPage.Navigation.PushAsync(new SelectCharacter());


                //await Application.Current.MainPage.DisplayAlert("SORRY", "I can't restart properly yet", "OK");

                //close the notification
                game.NotificationActive = false;
                NotificationIsVisible = false;

                Page currentPage = App.Current.MainPage.Navigation.NavigationStack.LastOrDefault();
                Application.Current.MainPage.Navigation.InsertPageBefore(new SelectCharacter(), currentPage);
                await Application.Current.MainPage.Navigation.PopAsync();

            }
            else if (NotificationButtonText == "Play")
            {
                if (game.GameStatus == Game.Status.Stage1Completed)
                {
                    game.GameStatus = Game.Status.Stage2Active;
                    //TODO activate new stage 2 features (job games)
                }
                else if (game.GameStatus == Game.Status.Stage2Completed)
                {
                    game.GameStatus = Game.Status.Stage3Active;
                    //TODO activate new stage 3 features (investment options)
                }
                //close the notification
                game.NotificationActive = false;
                NotificationIsVisible = false;
                //restart timer
                game.IsTimerCancel = false;
                game.StartTimer();
            }
        }

        public string RoundTimer
        {
            set
            {
                if (roundTimer != value)
                {
                    roundTimer = value;

                    OnPropertyChanged(nameof(RoundTimer));
                    //update the round timer
                }
            }
            get
            {
                return game.TimerText;
            }
        }
        public double Happiness
        {
            set
            {
                if(game.Happiness != value)
                {
                    game.Happiness = value;
                    OnPropertyChanged(nameof(Happiness));
                }
            }
            get
            {
                return game.Happiness;
            }
        }
        public double MoneyProgress
        {
            set
            {
                if (game.MoneyProgress != value)
                {
                    OnPropertyChanged(nameof(MoneyProgress));
                }
            }
            get
            {
                return game.MoneyProgress;
            }
        }

        public double HappinessProgress
        {
            set
            {
                if (game.HappinessProgress != value)
                {
                    OnPropertyChanged(nameof(HappinessProgress));
                }
            }
            get
            {
                return game.HappinessProgress;
            }
        }

        public double MoneyTotal
        {
            set
            {
                if (game.MoneyTotal != value)
                {
                    game.MoneyTotal = value;
                    OnPropertyChanged(nameof(MoneyTotal));
                }
            }
            get
            {
                return game.MoneyTotal;
            }
        }

        public double MoneyGoal
        {
            set
            {
                if (game.MoneyGoal != value)
                {
                    game.MoneyGoal = value;
                    OnPropertyChanged(nameof(MoneyGoal));
                }
            }
            get
            {
                return game.MoneyGoal;
            }
        }
        public bool JobGamesUnlocked
        {
            get
            {
                Application.Current.MainPage.DisplayAlert("Code Check", "it's in get method", "Alright");
                //TODO remove stage 1 from this if statement!! For testing purposes only
                if ((game.GameStage == Game.Stage.Two) || (game.GameStage == Game.Stage.Three) || (game.GameStage == Game.Stage.One))
                {
                    Application.Current.MainPage.DisplayAlert("Code Check", "returning TRUE", "Alright");
                    return true;
                }
                else
                {
                    Application.Current.MainPage.DisplayAlert("Code Check", "returning FALSE", "Alright");
                    return false;
                }
            }
            set
            {
                if (jobGamesUnlocked != value)
                {
                    jobGamesUnlocked = value;

                    OnPropertyChanged(nameof(JobGamesUnlocked));
                }
            }
        }

        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
