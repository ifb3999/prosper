﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.
    
    Forms.Xaml;

namespace prosper
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Achievement : ContentPage
    {
        public Achievement()
        {
            InitializeComponent();
        }

        async void OnRoomSelection(object sender, EventArgs args)
        {
            Button button = (Button)sender;
            //set character to char 1
            //Game.Instance.Character = 1;
            //open my room
            Page currentPage = App.Current.MainPage.Navigation.NavigationStack.LastOrDefault();
            Application.Current.MainPage.Navigation.InsertPageBefore(new MyRoom(), currentPage);
            await Application.Current.MainPage.Navigation.PopAsync();
        }

        async void OnHelpSelection(object sender, EventArgs args)
        {
            Button button = (Button)sender;
            //set character to char 1
            //Game.Instance.Character = 1;
            //open my room
            Page currentPage = App.Current.MainPage.Navigation.NavigationStack.LastOrDefault();
            Application.Current.MainPage.Navigation.InsertPageBefore(new Help(), currentPage);
            await Application.Current.MainPage.Navigation.PopAsync();
        }

        async void OnPlayTutorial(object sender, EventArgs args)
        {
            //when finished tutorial
            //game.GameInitialised = true;
            //TutorialPopupVisible = false;
            //temporary
            Application.Current.MainPage.DisplayAlert("Tutorial", "...awaiting implementation", "OK");
            await Application.Current.MainPage.Navigation.PopAsync();

        }

    }
}