﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace prosper
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Grass : ContentView
    {
        public Game game = Game.Instance;
        Image characterImage;

        public Random rd = new Random();
        public int x, y;
        public int width = 11, height = 5;
        public int direc = 0;
        Image[,] imageArr = new Image[11, 6];
        double xx, yy;
        StackLayout stackLayout;
        private double translatedX = 0, translatedY = 0;
        int[] stone_x = new int[5];
        int[] stone_y = new int[5];
        int num_of_stones;

        public int count = 0;

        public Grass()
		{
			InitializeComponent ();
            InitilizeGame();
            var panGesture = new PanGestureRecognizer();
            panGesture.PanUpdated += (s, e) =>
            {
                // Handle the pan
            };
            characterImage.GestureRecognizers.Add(panGesture);
            panGesture.PanUpdated += OnMoveChar;
            characterImage.GestureRecognizers.Add(panGesture);
        }

        void InitilizeGame()
        {
            x = rd.Next(0, height-1);
            y = rd.Next(0, width);

            while ((x == 0 && y==0) || (x==0 && y==1))
            {
                x = rd.Next(0, height - 1);
                y = rd.Next(0, width);
            }

            characterImage = new Image
            {
                Source = "chucaoji2.png",
                Aspect = Aspect.AspectFill,
            };

            for (int i = 0; i < 5; i++)
            {
                stone_x[i] = rd.Next(0, height);
                stone_y[i] = rd.Next(0, width);
            }

            num_of_stones = rd.Next(3, 6);

            for (int i = 0; i < height; i++)
                for (int j = 0; j < width; j++)
                {
                    imageArr[j, i] = new Image
                    {
                        Source = "grass10.png",
                        Aspect = Aspect.AspectFill,
                    };
                    bool isStone = false;
                    for (int k = 0; k < num_of_stones; k++)
                    {
                        if (i == stone_x[k] && j == stone_y[k])
                        {
                            isStone = true;
                            break;
                        }
                    }
                    if (isStone) imageArr[j, i].IsVisible = false ;
                }



            for (int i = 0; i < height; i++)
            {
                for (int j = 0; j < width; j++)
                {
                    //if (i >= 1 && i <= 4 && j >= 4 && j <= 7)
                    //    continue;

                    if ((x == i && y == j) || (i == 0 && j == 0) || (i == 0 && j == 1))
                    {
                        continue;
                    }

                    bool ok = false;
                    for (int k = 0; k < num_of_stones; k++)
                    {
                        if (i == stone_x[k] && j == stone_y[k])
                        {
                            (Content as Grid).Children.Add(new Image { Source = "stone6.png" }, j, i);
                            ok = true;
                        }
                    }

                    if (ok) continue;

                    (Content as Grid).Children.Add(imageArr[j, i], j, i);
                }
            }

            stackLayout = new StackLayout
            {
                Children = { characterImage }
            };

            Grid.SetRow(stackLayout, x);
            Grid.SetColumn(stackLayout, y);
            (Content as Grid).Children.Add(stackLayout);
        }

        void MoveCharacter(int ex, int ey)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                int dx = ex, dy = ey;

                if (!(dx == 0 || dy == 0))
                    return;

                if (x + dx >= 0 && x + dx < height && y + dy >= 0 && y + dy < width)
                {
                    for (int k = 0; k < num_of_stones; k++)
                    {
                        if (stone_x[k] == x + dx && stone_y[k] == y + dy)
                        {
                            // hit rock
                            game.hitRock = true;
                            break;
                        }
                    }

                    if (game.hitRock)
                        return;

                    if ((x + dx == 0) && (y + dy == 0 || y + dy == 1))
                        return;
  
                    Grid.SetRow(stackLayout, x + dx);
                    Grid.SetColumn(stackLayout, y + dy);

                    imageArr[y + dy, x + dx].IsVisible = false;
                    x += dx;
                    y += dy;
                    //await Task.Delay(200);
                }

                for (int i = 0; i < height; i++)
                {
                    for (int j = 0; j < width; j++)
                    {
                        if ((i == 0 && j == 0) || (i == 0 && j == 1))
                            continue;
                        bool isStone = false;
                        for (int k = 0; k < num_of_stones; k++)
                        {
                            if (i == stone_x[k] && j == stone_y[k])
                            {
                                isStone = true;
                                break;
                            }
                        }
                        if (isStone) continue;
                        if (imageArr[j, i].IsVisible == true)
                            return;
                    }
                }
                game.mowGameFinished = true;
            });
        }

        void OnMoveChar(object sender, PanUpdatedEventArgs e)
        {
            //Console.WriteLine("move");
            //(Content as Grid).GetValue(TouchPoint1);
            
         
            xx = y;
            yy = x;

            switch (e.StatusType)
            {
                case GestureStatus.Running:
                    translatedX = e.TotalX;
                    translatedY = e.TotalY;
                    // Translate and ensure we don't pan beyond the wrapped user interface element bounds.
                    stackLayout.TranslationX =
                       //Math.Max(Math.Min(0, x + e.TotalX), -Math.Abs(stackLayout.Width -1920));
                       xx + e.TotalX;
                    stackLayout.TranslationY =
                       //Math.Max(Math.Min(0, y + e.TotalY), -Math.Abs(stackLayout.Height -1080));
                       yy + e.TotalY;
                    count = 1;
                    break;

                case GestureStatus.Completed:
                    if (count == 1)
                    {
                        stackLayout.TranslationX = xx;
                        stackLayout.TranslationY = yy;

                        if (translatedX < 0 && Math.Abs(translatedX) > Math.Abs(translatedY))
                        {
                            MoveCharacter(0, -1);
                        }
                        else if (translatedX > 0 && translatedX > Math.Abs(translatedY))
                        {
                            MoveCharacter(0, 1);
                        }
                        else if (translatedY < 0 && Math.Abs(translatedY) > Math.Abs(translatedX))
                        {
                            MoveCharacter(-1, 0);
                        }
                        else if (translatedY > 0 && translatedY > Math.Abs(translatedX))
                        {
                            MoveCharacter(1, 0);
                        }
                        count = 0;
                    }

                    
                    // Store the translation applied during the pan
                    //xx = stackLayout.TranslationX;
                    //yy = stackLayout.TranslationY;
                    //int xi = (int)(xx / 35.0);
                    //int yi = (int)(yy / 35.0);
                    //if ((y + xi) >= 0 && (y + xi) <= 10 && (x + yi + 1) >= 0 && (x + yi + 1) <= 4)
                    //{
                    //    imageArr[y + xi, x + yi + 1].IsVisible = false;
                    //    Console.WriteLine(xi + ":" + y + xi);
                    //    Console.WriteLine(yi + ":" + x + yi + 1);
                    //}
                    break;
            }
            Console.WriteLine("x: " + xx);
            Console.WriteLine("y: " + yy);

        }

    }
}