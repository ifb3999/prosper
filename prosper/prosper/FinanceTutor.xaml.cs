﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace prosper
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class FinanceTutor : ContentPage
	{
        public FinanceTutor()
        {
            this.BindingContext = new FinanceTutorViewModel();
            InitializeComponent();
            MessagingCenter.Subscribe<FinanceTutorViewModel, FinanceQuestion>(this, "Display Question", (sender, question) =>
            {
                //cleanup last screens
                ExplanationTextLabel.IsVisible = false;
                NextQuestionButton.IsVisible = false;
                OutcomeImage.IsVisible = false;
                
                
                //make the question elements visible
                QuestionTextLabel.IsVisible = true;
                AnswerButtonsGridLayout.IsVisible = true;
                //add question text
                QuestionTextLabel.Text = question.Question;
                //add answer buttons
                string[] answers = question.JumbledAnswers();
                Button answer1 = new Button { FontSize=8 };
                Button answer2 = new Button { FontSize = 8};
                Button answer3 = new Button { FontSize = 8 };
                Button answer4 = new Button { FontSize = 8 };
                //place answer buttons in grid
                if (answers.Length == 2)
                {
                    answer1.Text = answers[0];
                    answer2.Text = answers[1];
                    AnswerButtonsGridLayout.Children.Add(answer1, 0, 0);
                    AnswerButtonsGridLayout.Children.Add(answer2, 1, 0);
                    //Grid.SetRowSpan(answer1, 2);
                    //Grid.SetRowSpan(answer2, 2);

                }
                else if (answers.Length == 3)
                {
                    answer1.Text = answers[0];
                    answer2.Text = answers[1];
                    answer3.Text = answers[2];
                    AnswerButtonsGridLayout.Children.Add(answer1, 0, 0);
                    AnswerButtonsGridLayout.Children.Add(answer2, 0, 1);
                    AnswerButtonsGridLayout.Children.Add(answer3, 1, 0);
                    answer1.Margin = -3;
                    answer2.Margin = -3;
                    answer3.Margin = -3;
                }
                else if (answers.Length == 4)
                {
                    answer1.Text = answers[0];
                    answer2.Text = answers[1];
                    answer3.Text = answers[2];
                    answer4.Text = answers[3];
                    AnswerButtonsGridLayout.Children.Add(answer1, 0, 0);
                    AnswerButtonsGridLayout.Children.Add(answer2, 0, 1);
                    AnswerButtonsGridLayout.Children.Add(answer3, 1, 0);
                    AnswerButtonsGridLayout.Children.Add(answer4, 1, 1);
                    answer1.Margin = -3;
                    answer2.Margin = -3;
                    answer3.Margin = -3;
                    answer4.Margin = -3;
                }

                answer1.SetBinding(Button.CommandProperty, "AnswerQuestionCommand");
                answer1.CommandParameter = answer1.Text;
                answer2.SetBinding(Button.CommandProperty, "AnswerQuestionCommand");
                answer2.CommandParameter = answer2.Text;
                answer3.SetBinding(Button.CommandProperty, "AnswerQuestionCommand");
                answer3.CommandParameter = answer3.Text;
                answer4.SetBinding(Button.CommandProperty, "AnswerQuestionCommand");
                answer4.CommandParameter = answer4.Text;
                //answer1.CommandParameter = new Binding("answer1.Text");
                //answer1.SetBinding(Button.CommandParameterProperty, new Binding("answer1.Text"));
                //answer1.SetBinding(Button)
            });
            MessagingCenter.Subscribe<FinanceTutorViewModel, string>(this, "Display Explanation", (sender, explanation) =>
            {
                //cleanup last screens
                AnswerButtonsGridLayout.Children.Clear();
                QuestionTextLabel.IsVisible = false;
                AnswerButtonsGridLayout.IsVisible = false;

                ExplanationTextLabel.Text = explanation;
                ExplanationTextLabel.IsVisible = true;
                OutcomeImage.IsVisible = true;
                NextQuestionButton.IsVisible = true;

            });
        }
        protected override void OnDisappearing()
        {
            base.OnDisappearing();
            MessagingCenter.Unsubscribe<FinanceTutorViewModel, FinanceQuestion>(this, "Display Question");
            MessagingCenter.Unsubscribe<FinanceTutorViewModel, string>(this, "Display Explanation");
        }

    }
}