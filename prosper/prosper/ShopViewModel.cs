﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;


namespace prosper
{
    class ShopViewModel : INotifyPropertyChanged
    {
        bool sideBarVisible = false;
        bool warningVisible = false;
        bool confirmationVisible = false;
        bool noMoneyVisible = false;
        public Game game = Game.Instance;
        public event PropertyChangedEventHandler PropertyChanged;
        public ICommand LockedGoodsCommand { get; private set; }
        public ICommand CloseNoMoneyCommand { get; private set; }
        public ICommand CloseLockedGoodsCommand { get; private set; }
        public ICommand BuyGoodsCommand { get; private set; }
        public ICommand CloseConfirmationCommand { get; private set; }
        public ICommand SideBarCommand { get; private set; }
        public ICommand SideBarDisappear { get; private set; }
        public Object OneData { get; set; }

        public bool WarningVisible
        {
            get
            {
                return warningVisible;
            }
            set
            {
                warningVisible = value;
                OnPropertyChanged(nameof(WarningVisible));
            }
        }

        public bool ConfirmationVisible
        {
            get
            {
                return confirmationVisible;
            }
            set
            {
                confirmationVisible = value;
                OnPropertyChanged(nameof(ConfirmationVisible));
            }
        }

        public bool NoMoneyVisible
        {
            get
            {
                return noMoneyVisible;
            }
            set
            {
                noMoneyVisible = value;
                OnPropertyChanged(nameof(NoMoneyVisible));
            }
        }

        public bool SideBarVisible
        {
            get
            {
                return sideBarVisible;
            }
            set
            {
                sideBarVisible = value;
                OnPropertyChanged(nameof(SideBarVisible));
            }
        }

        public ShopViewModel()
        {

            LockedGoodsCommand = new Command(OnLockedGoodsSelect);
            CloseNoMoneyCommand = new Command(CloseNoMoney);
            CloseLockedGoodsCommand = new Command(CloseLockedGoods);
            BuyGoodsCommand = new Command<string>((key) =>
            {
                if (game.Prices[key][0] <= game.MoneyTotal)
                {
                    if (game.GoodsNumber < 9)
                    {
                        game.GoodsNumber = game.GoodsNumber+1;
                    }
                    else
                    {
                        game.GoodsNumber=0;
                    }
                    ConfirmationVisible = true;
                    Game.Instance.Goods = key;
                    game.GoodsMins[game.GoodsNumber] = game.Prices[key][2];
                    game.GoodsCounter[game.GoodsNumber] = game.Prices[key][3];
                }
                else
                {
                    NoMoneyVisible = true;
                }
            });
            CloseConfirmationCommand = new Command(CloseConfirmation);
            SideBarCommand = new Command(OnSideBarSelect);
            SideBarDisappear = new Command(OnSideBarNotSelect);
        }

        void OnLockedGoodsSelect()
        {
            WarningVisible = true;
        }


        void CloseLockedGoods()
        {
            //set random event popup invisible
            WarningVisible = false;
        }

        void CloseNoMoney()
        {
            NoMoneyVisible = false;
        }

        void OnBuyGoodsSelect()
        {
            ConfirmationVisible = true;
        }


        void CloseConfirmation()
        {
            //set random event popup invisible
            ConfirmationVisible = false;
        }

        void OnSideBarSelect()
        {

            SideBarVisible = true;
        }
        void OnSideBarNotSelect()
        {

            SideBarVisible = false;
        }

        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
