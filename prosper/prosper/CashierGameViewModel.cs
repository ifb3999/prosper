﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using Xamarin.Forms;
using System.Windows.Input;
using System.Linq;

namespace prosper
{
    public class CashierGameViewModel : INotifyPropertyChanged
    {
        public Game game = Game.Instance;

        public event PropertyChangedEventHandler PropertyChanged;
        bool sideBarVisible = false;
        bool beginPageVisible = true;
        bool paymentVisible = false;
        bool changeVisible = false;
        bool proceedNextCustomerVisible = false;
        bool countErrorVisible = false;
        bool cashierGameCongratulationVisible = false;

        string halfCharImageSource;
        string numberOnCalculator = "0.00";
        int answer = 0;
        int CalNumber = 0;
        int numOfCorrectCounting = 0;
        Random rd = new Random();
        int numOfGoods;
        string lastOperator = "+";
        string goodsWithLabelSource = "applewithlabel.png";
        string[] goodsWithLabel;
        string miniRoundTimer;
        string cashierGameEarnedMoney;
        int[] numOfMoneyNotes;
        int correct = 1;
        double[] moneyNote;
        double totalPaymentNum = 0.0;
        double totalChanges = 0.0;
        string totalPayment;

        public ICommand SideBarCommand { get; private set; }
        public ICommand SideBarDisappear { get; private set; }
        public ICommand TapToBegin { get; private set; }
        public ICommand ChooseNumber { get; private set; }
        public ICommand ChooseOperator { get; private set; }
        public ICommand CountTotal { get; private set; }
        public ICommand PayMoneyNote { get; private set; }
        public ICommand ChangeClear { get; private set; }
        public ICommand ChangeDone { get; private set; }
        public ICommand ClearNumber { get; private set; }
        public ICommand PayMoney { get; private set; }

        public CashierGameViewModel()
        {
            //PaymentVisible = false;
            //ChangeVisible = true;

            goodsWithLabel = new string[4];
            goodsWithLabel[0] = "applewithlabel.png";
            goodsWithLabel[1] = "bookwithlabel.png";
            goodsWithLabel[2] = "hatwithlabel.png";
            goodsWithLabel[3] = "watermelonwithlabel.png";
            sideBarVisible = false;
            beginPageVisible = true;
            paymentVisible = false;
            changeVisible = false;
            proceedNextCustomerVisible = false;
            countErrorVisible = false;
            cashierGameCongratulationVisible = false;
            ProceedNextCustomerVisible = false;
            CountErrorVisible = false;
            CashierGameCongratulationVisible = false;

            numOfGoods = rd.Next(3, 5);
            TotalPayment = "0.00";

            numOfMoneyNotes = new int[9];
            moneyNote = new double[9];
            for (int i = 0; i < 9; i++)
            {
                numOfMoneyNotes[i] = 0;
            }

            moneyNote[0] = 50.00; moneyNote[1] = 10.00; moneyNote[2] = 5.00; moneyNote[3] = 1.00;
            moneyNote[4] = 0.01; moneyNote[5] = 0.50; moneyNote[6] = 0.05; moneyNote[7] = 0.10;


            SideBarCommand = new Command(OnSideBarSelect);
            SideBarDisappear = new Command(OnSideBarNotSelect);
            TapToBegin = new Command(OnCashierBeginSelect);
            ChangeClear = new Command(OnChangeClear);
            ChangeDone = new Command(OnChangeDone);
            ClearNumber = new Command(OnClearNumber);
            PayMoney = new Command(OnPayMoney);

            ChooseNumber = new Command<string>((key) =>
            {
                CalNumber = CalNumber*10+int.Parse(key);
                NumberOnCalculator = string.Format("{0}{1}", CalNumber, ".00");
            });

            ChooseOperator = new Command<string>((key) =>
            {
                if (numOfGoods > 0)
                {
                    lastOperator = key;
                    switch (lastOperator)
                    {
                        case "+":
                            answer += CalNumber;
                            CalNumber = 0;
                            break;
                        case "-":
                            answer -= CalNumber;
                            CalNumber = 0;
                            break;
                        default:
                            break;
                    }
                    Random rnd = new Random();
                    int a = rnd.Next(0, 4);

                    Console.WriteLine(a);
                    if (numOfGoods == 1)
                        GoodsWithLabelSource = "";
                    else
                        GoodsWithLabelSource = goodsWithLabel[a];

                    numOfGoods--;
                }
            });

            PayMoneyNote = new Command<string>((key) =>
            {
                if (PaymentVisible)
                {
                    int index = int.Parse(key);
                    NumOfMoneyNotes[index]++;
                    totalPaymentNum += moneyNote[index];
                    TotalPayment = string.Format("${0:F}", totalPaymentNum);
                    OnPropertyChanged(nameof(NumOfMoneyNotes));
                } else if (ChangeVisible)
                {
                    int index = int.Parse(key);
                    NumOfMoneyNotes[index]++;
                    totalChanges += moneyNote[index];
                    OnPropertyChanged(nameof(NumOfMoneyNotes));
                }
            });

            CountTotal = new Command(OnTotalSelect);

            AddHalfCharacterToScreen(game.Character);

            int mins = 2;
            int counter = 0;
            Device.StartTimer(TimeSpan.FromSeconds(1), () =>
            {
                Device.BeginInvokeOnMainThread(() =>
                {
                    counter = counter - 1;
                    if (counter < 0)
                    {
                        counter = 59;
                        mins = mins - 1;
                        if (mins < 0)
                        {
                            mins = 0;
                            counter = 0;
                        }
                    }
                    MiniRoundTimer = string.Format("{0:00}:{1:00}", mins, counter);
                });
                if (mins == 0 && counter == 0)
                {
                    // end game
                    CashierGameCongratulationVisible = true;
                    CashierGameEarnedMoney = string.Format("You earned ${0}", 5 * numOfCorrectCounting);
                    game.MoneyTotal += 5 * numOfCorrectCounting;
                    game.GameTransactions.Add(new Transaction(5 * numOfCorrectCounting, "Cashier Job"));
                    return false;
                }
                else
                    return true;

            });
        }

        public string MiniRoundTimer
        {
            set
            {
                miniRoundTimer = value;
                OnPropertyChanged(nameof(MiniRoundTimer));
            }
            get
            {
                return miniRoundTimer;
            }
        }

        public string CashierGameEarnedMoney
        {
            set
            {
                cashierGameEarnedMoney = value;
                OnPropertyChanged(nameof(CashierGameEarnedMoney));
            }
            get
            {
                return cashierGameEarnedMoney;
            }
        }

        public bool SideBarVisible
        {
            get
            {
                return sideBarVisible;
            }
            set
            {
                sideBarVisible = value;
                OnPropertyChanged(nameof(SideBarVisible));
            }
        }

        public bool BeginPageVisible
        {
            get
            {
                return beginPageVisible;
            }
            set
            {
                beginPageVisible = value;
                OnPropertyChanged(nameof(BeginPageVisible));
            }
        }

        public bool PaymentVisible
        {
            get
            {
                return paymentVisible;
            }
            set
            {
                paymentVisible = value;
                OnPropertyChanged(nameof(PaymentVisible));
            }
        }

        public bool ChangeVisible
        {
            get
            {
                return changeVisible;
            }
            set
            {
                changeVisible = value;
                OnPropertyChanged(nameof(ChangeVisible));
            }
        }

        public string HalfCharImageSource
        {
            get
            {
                return halfCharImageSource;
            }
            set
            {
                halfCharImageSource = value;
                OnPropertyChanged(nameof(HalfCharImageSource));
            }
        }

        public string NumberOnCalculator
        {
            get
            {
                return numberOnCalculator;
            }
            set
            {
                numberOnCalculator = value;
                OnPropertyChanged(nameof(NumberOnCalculator));
            }
        }

        public string TotalPayment
        {
            get
            {
                return totalPayment;
            }
            set
            {
                totalPayment = value;
                OnPropertyChanged(nameof(TotalPayment));
            }
        }

        public string GoodsWithLabelSource
        {
            get
            {
                return goodsWithLabelSource;
            }
            set
            {
                goodsWithLabelSource = value;
                OnPropertyChanged(nameof(GoodsWithLabelSource));
            }
        }

        public bool ProceedNextCustomerVisible
        {
            get
            {
                return proceedNextCustomerVisible;
            }
            set
            {
                proceedNextCustomerVisible = value;
                OnPropertyChanged(nameof(ProceedNextCustomerVisible));
            }
        }

        public bool CountErrorVisible
        {
            get
            {
                return countErrorVisible;
            }
            set
            {
                countErrorVisible = value;
                OnPropertyChanged(nameof(CountErrorVisible));
            }
        }

        public bool CashierGameCongratulationVisible
        {
            get
            {
                return cashierGameCongratulationVisible;
            }
            set
            {
                cashierGameCongratulationVisible = value;
                OnPropertyChanged(nameof(CashierGameCongratulationVisible));
            }
        }

        public int[] NumOfMoneyNotes
        {
            get
            {
                return numOfMoneyNotes;
            }
            set
            {
                numOfMoneyNotes = value;
                OnPropertyChanged(nameof(NumOfMoneyNotes));
            }
        }

        void OnChangeClear()
        {
            for (int i=0; i<8; i++)
            {
                NumOfMoneyNotes[i] = 0;
            }
            totalChanges = 0.0;
            OnPropertyChanged(nameof(NumOfMoneyNotes));
        }

        void OnClearNumber()
        {
            NumberOnCalculator = "0";
            CalNumber = 0;
        }

        void OnPayMoney()
        {
            PaymentVisible = false;
            ChangeVisible = true;

            // Show Change Money
            double changeMoney = totalPaymentNum - answer;
            for (int i = 0; i < 8; i++)
            {
                NumOfMoneyNotes[i] = 0;
            }
            OnPropertyChanged(nameof(NumOfMoneyNotes));
            NumberOnCalculator = string.Format("Change:{0:F}", changeMoney);
        }

        void ProceedNextCustomer()
        {
            int counter = 2;
            ProceedNextCustomerVisible = true;
            Device.StartTimer(TimeSpan.FromSeconds(1), () =>
            {
                Device.BeginInvokeOnMainThread(() =>
                {
                    counter = counter - 1;
                    if (counter < 0)
                    {
                        counter = 0;
                    }
                });
                if (counter == 0)
                {
                    ProceedNextCustomerVisible = false;
                    ChangeVisible = false;
                    numOfGoods = rd.Next(3, 5);

                    answer = 0;
                    TotalPayment = "0.00";
                    totalPaymentNum = 0;
                    totalChanges = 0;
                    NumberOnCalculator = "0.00";
                    for (int i = 0; i < 8; i++)
                    {
                        NumOfMoneyNotes[i] = 0;
                    }
                    OnPropertyChanged(nameof(NumOfMoneyNotes));

                    Random rnd = new Random();
                    int a = rnd.Next(0, 4);
                    GoodsWithLabelSource = goodsWithLabel[a];
                    return false;
                }
                else
                    return true;

            });
        }

        void OnChangeDone()
        {
            // check calculation
            double changeMoney = totalPaymentNum - answer;
            double inputMoney = totalChanges;

            Console.WriteLine(changeMoney + ":" + inputMoney);
            if (changeMoney != inputMoney)
            {
                // Display counting error
                CountErrorVisible = true;
                totalChanges = 0.0;
                correct = 0;
            }
            else
            {
                numOfCorrectCounting+= correct;
                correct = 1;
                CountErrorVisible = false;
                //display messange and proceed to next customer
                ProceedNextCustomer();
            }
        }



        void OnSideBarSelect()
        {

            SideBarVisible = true;
        }
        void OnSideBarNotSelect()
        {

            SideBarVisible = false;
        }


        void OnCashierBeginSelect()
        {

            BeginPageVisible = false;
        }

        void OnTotalSelect()
        {
            switch (lastOperator)
            {
                case "+":
                    answer += CalNumber;
                    CalNumber = 0;
                    break;
                case "-":
                    answer -= CalNumber;
                    CalNumber = 0;
                    break;
                default:
                    break;
            }
            NumberOnCalculator = string.Format("Total:{0}{1}", answer.ToString(), ".00");
            PaymentVisible = true;
        }

        void AddHalfCharacterToScreen(int characterNum)
        {
            switch (characterNum)
            {
                case 1:
                    HalfCharImageSource = "char1half.png";
                    break;
                case 2:
                    HalfCharImageSource = "char2half.png";
                    break;
                case 3:
                    HalfCharImageSource = "char3half.png";
                    break;
                default:
                    //should throw an error I think
                    break;
            }
        }

        //public string MiniRoundTimer
        //{
        //    set
        //    {
        //        miniRoundTimer = value;
        //        OnPropertyChanged(nameof(MiniRoundTimer));
        //    }
        //    get
        //    {
        //        return miniRoundTimer;
        //    }
        //}



        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
