﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace prosper
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class MyRoom : ContentPage
	{

        public MyRoom ()
		{
            InitializeComponent();
        }

        async void OnShopSelect()
        {
            await Navigation.PushAsync(new Shop());
        }

        async void OnEarnMoneySelect()
        {

            await Navigation.PushAsync(new EarnMoney());
        }
        async void OnManageMoneySelect()
        {
            await Navigation.PushAsync(new ManageMoney());
        }

        async void OnAchievementSelection(object sender, EventArgs args)
        {
            Button button = (Button)sender;
            Page currentPage = App.Current.MainPage.Navigation.NavigationStack.LastOrDefault();
            Application.Current.MainPage.Navigation.InsertPageBefore(new Achievement(), currentPage);
            await Application.Current.MainPage.Navigation.PopAsync();
        }

        async void OnHelpSelection(object sender, EventArgs args)
        {
            Button button = (Button)sender;
            Page currentPage = App.Current.MainPage.Navigation.NavigationStack.LastOrDefault();
            Application.Current.MainPage.Navigation.InsertPageBefore(new Help(), currentPage);
            await Application.Current.MainPage.Navigation.PopAsync();
        }

        async void OnPlayTutorial(object sender, EventArgs args)
        {
            Application.Current.MainPage.DisplayAlert("Tutorial", "...awaiting implementation", "OK");
            await Application.Current.MainPage.Navigation.PopAsync();

        }



    }
}