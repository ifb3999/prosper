﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using Xamarin.Forms;
using System.Windows.Input;
using System.Linq;

namespace prosper
{
    public class MowGameViewModel : INotifyPropertyChanged
    {
        public Game game = Game.Instance;
        string miniRoundTimer;
        bool isDisplayMessage;
        bool congratulationVisible;
        bool introductionVisible;
        string earnedMoney;
        public event PropertyChangedEventHandler PropertyChanged;
        bool sideBarVisible = false;
        int money;
        public ICommand SideBarCommand { get; private set; }
        public ICommand SideBarDisappear { get; private set; }
        public ICommand StartMowGame { get; private set; }
        //public double temp;

        public MowGameViewModel()
        {
            //temp = game.MoneyTotal;
            //game.MoneyTotal = 30;
            SideBarCommand = new Command(OnSideBarSelect);
            SideBarDisappear = new Command(OnSideBarNotSelect);
            StartMowGame = new Command(OnStartMowGame);
            Device.StartTimer(TimeSpan.FromSeconds(1), () =>
            {
                OnPropertyChanged(nameof(EarnedMoney));
                OnPropertyChanged(nameof(MiniRoundTimer));
                if (game.hitRock)
                {
                    if (money > 5)
                        money -= 5;
                    DisplayMessage();
                    game.hitRock = false;
                }
                return true;
            });
            IsDisplayMessageBool = false;
            IntroductionVisible = true;
            CongratulationVisible = false;
        }

        public void MiniStartTimer()
        {
            CongratulationVisible = false;
            int counter = 30;
            int mins = 0;
            money = 30;
            Device.StartTimer(new TimeSpan(0, 0, 1), () =>
            {
                Device.BeginInvokeOnMainThread(() =>
                {
                    counter = counter - 1;
                    if (counter < 0)
                    {
                        counter = 0;
                    }

                    miniRoundTimer = string.Format("{0:00}:{1:00}", mins, counter);
                });
                if (counter == 0)
                {
                    //will need to deduct bills depeneding on stage
                    money -= 5;
                    //restart the timer again
                    DisplayMessage();

                    return false;
                }
                else
                {
                    return true;
                }

            });

            Device.StartTimer(new TimeSpan(0, 0, 1), () =>
            {
                if (game.mowGameFinished)
                {
                    CongratulationVisible = true;
                    EarnedMoney = string.Format("You earned ${0}", money);
                    game.MoneyTotal += money;
                    game.GameTransactions.Add(new Transaction(money, "Mowing Lawn Pocket Money"));
                    game.mowGameFinished = false;
                    return false;
                }
                return true;
            });

           
        }

        public void DisplayMessage()
        {
            IsDisplayMessageBool = true;
            int mins = 0;
            int counter = 3;
            Device.StartTimer(new TimeSpan(0, 0, 1), () =>
            {
                Device.BeginInvokeOnMainThread(() =>
                {
                    counter = counter - 1;
                    if (counter < 0)
                    {
                        counter = 0;
                    }
                });
                if (mins == 0 && counter == 0)
                {
                    IsDisplayMessageBool = false;
                    return false;
                }
                else
                {
                    return true;
                }
            });
        }


        public bool SideBarVisible
        {
            get
            {
                return sideBarVisible;
            }
            set
            {
                sideBarVisible = value;
                OnPropertyChanged(nameof(SideBarVisible));
            }
        }

        public bool IntroductionVisible
        {
            get
            {
                return introductionVisible;
            }
            set
            {
                introductionVisible = value;
                OnPropertyChanged(nameof(IntroductionVisible));
            }
        }

        void OnSideBarSelect()
        {

            SideBarVisible = true;
        }
        void OnSideBarNotSelect()
        {

            SideBarVisible = false;
        }

        void OnStartMowGame()
        {
            IntroductionVisible = false;
            MiniStartTimer();
        }

        public string MiniRoundTimer
        {
            set
            {
                miniRoundTimer = value;
                OnPropertyChanged(nameof(MiniRoundTimer));
            }
            get
            {
                return miniRoundTimer;
            }
        }

        public string EarnedMoney
        {
            set
            {
                earnedMoney = value;
                OnPropertyChanged(nameof(EarnedMoney));
            }
            get
            {
                return earnedMoney;
            }
        }

        public bool CongratulationVisible
        {
            get
            {
                return congratulationVisible;
            }
            set
            {
                congratulationVisible = value;
                OnPropertyChanged(nameof(CongratulationVisible));
            }
        }

        public bool IsDisplayMessageBool
        {
            set
            {
                isDisplayMessage = value;

                OnPropertyChanged(nameof(IsDisplayMessageBool));

            }
            get
            {
                return isDisplayMessage;
            }
        }

        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
