﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using Xamarin.Forms;
using System.Windows.Input;
using System.Linq;
using Android.OS;
using Android.Content;

namespace prosper
{
    class CleanTheHouseViewModel : INotifyPropertyChanged
    {
        public Game game = Game.Instance;
        string miniRoundTimer;
        bool notificationPopupVisible = false;
        string notificationPopupText;
        string notificationPopupTitle;
        bool instructionsVisible = true;
        bool watermelon1Visible = true;
        bool watermelon2Visible = true;
        bool watermelon3Visible = true;
        bool watermelon4Visible = true;
        bool laundry5Visible = true;
        bool laundry6Visible = true;
        bool laundry7Visible = true;
        bool book8Visible = true;
        bool book9Visible = true;
        bool book10Visible = true;
        bool sideBarVisible = false;
        string basketHighlight = "SaddleBrown";
        string binHighlight = "SaddleBrown";
        string bookshelfHighlight = "SaddleBrown";
        public event PropertyChangedEventHandler PropertyChanged;
        public ICommand SelectToolCommand { get; private set; }
        public ICommand UseToolCommand { get; private set; }
        public ICommand PlayButtonCommand { get; private set; }
        public ICommand MyRoomButtonCommand { get; private set; }
        public ICommand SideBarCommand { get; private set; }
        public ICommand SideBarDisappear { get; private set; }

        //check for completion
        CleanTheHouseGame miniGame = new CleanTheHouseGame();
        public CleanTheHouseViewModel()
        {
            //sideBarVisible = false;
            SelectToolCommand = new Command<string>(OnSelectTool);
            UseToolCommand = new Command<string>(OnUseTool);
            PlayButtonCommand = new Command(StartGame);
            MyRoomButtonCommand = new Command(OnMyRoom);
            SideBarCommand = new Command(OnSideBarSelect);
            SideBarDisappear = new Command(OnSideBarNotSelect);
        }

        void StartGame()
        {
            miniGame.GameLost = false;
            miniGame.GameWon = false;
            miniGame.NumDirtyItems = 10;
            InstructionsVisible = false;
            NotificationPopupVisible = false;
            Watermelon1Visible = true;
            Watermelon2Visible = true;
            Watermelon3Visible = true;
            Watermelon4Visible = true;
            Laundry5Visible = true;
            Laundry6Visible = true;
            Laundry7Visible = true;
            Book8Visible = true;
            Book9Visible = true;
            Book10Visible = true;
            miniGame.IsTimerCancel = false;
            miniGame.StartTimer();
            Device.StartTimer(TimeSpan.FromSeconds(1), () =>
            {
                OnPropertyChanged(nameof(MiniRoundTimer));
                OnPropertyChanged(nameof(BookshelfHighlight));
                OnPropertyChanged(nameof(BasketHighlight));
                miniGame.CheckGameStatus();
                if (miniGame.GameWon)
                {
                    NotificationPopupTitle = "Room Cleaned!";
                    NotificationPopupText = "Congratulations, you have finished cleaning the room." +
                    "\nYou have earned $10 pocket money.";
                    NotificationPopupVisible = true;
                    game.MoneyTotal += 10;
                    game.GameTransactions.Add(new Transaction(10, "Clean the House Pocket Money"));
                    miniGame.IsTimerCancel = true;
                    return false;
                }
                if (miniGame.GameLost)
                {
                    NotificationPopupTitle = "Out of Time!";
                    NotificationPopupText = "You didn't clean up the room in time." +
                    "\nYou have not earned any pocket money.";
                    NotificationPopupVisible = true;
                    miniGame.IsTimerCancel = true;
                    return false;
                }
                return true;
            });
        }

        void OnSelectTool(string tool)
        {
            if (tool == "Bookshelf")
            {
                miniGame.SelectedTool = CleanTheHouseGame.Tool.Bookshelf;
                //also highlight the selected tool
                BookshelfHighlight = "LightYellow";
                //unhighlight
                BinHighlight = "SaddleBrown";
                BasketHighlight = "SaddleBrown";
            }
            else if (tool == "Basket")
            {
                miniGame.SelectedTool = CleanTheHouseGame.Tool.Basket;
                //also highlight the selected tool
                BasketHighlight = "LightYellow";
                //unhighlight
                BookshelfHighlight = "SaddleBrown";
                BinHighlight = "SaddleBrown";
            }
            else if (tool == "Bin")
            {
                miniGame.SelectedTool = CleanTheHouseGame.Tool.Bin;
                //also highlight the selected tool
                BinHighlight = "LightYellow";
                //unhighlight
                BookshelfHighlight = "SaddleBrown";
                BasketHighlight = "SaddleBrown";
            }

        }

        void OnUseTool(string messCodeString)
        {
            int messCode = Int32.Parse(messCodeString);
            string expectedTool = "None";
            //use the mess code to find the expected tool
            if (messCode<5 && messCode > 0)
            {
                expectedTool = "Bin";
            }
            else if (messCode>4 && messCode < 8)
            {
                expectedTool = "Basket";
            }
            else if (messCode > 7 && messCode < 11)
            {
                expectedTool = "Bookshelf";
            }
            if (expectedTool == miniGame.SelectedTool.ToString())
            {
                //make the mess invisible
                switch (messCode)
                {
                    case 1:
                        Watermelon1Visible = false;
                        break;
                    case 2:
                        Watermelon2Visible = false;
                        break;
                    case 3:
                        Watermelon3Visible = false;
                        break;
                    case 4:
                        Watermelon4Visible = false;
                        break;
                    case 5:
                        Laundry5Visible = false;
                        break;
                    case 6:
                        Laundry6Visible = false;
                        break;
                    case 7:
                        Laundry7Visible = false;
                        break;
                    case 8:
                        Book8Visible = false;
                        break;
                    case 9:
                        Book9Visible = false;
                        break;
                    case 10:
                        Book10Visible = false;
                        break;
                    default:
                        break;
                }
                //show some animation

                miniGame.NumDirtyItems -= 1;
            }
            else
            {
                Xamarin.Forms.Application.Current.MainPage.DisplayAlert("Wrong Tool!", " ", "OK");
                //give some feedback - noise, shake the mess;
            }
        }

        async void OnMyRoom()
        {
            //take user back to my room
            Page currentPage = App.Current.MainPage.Navigation.NavigationStack.LastOrDefault();
            //Application.Current.MainPage.Navigation.InsertPageBefore(new SelectCharacter(), currentPage);
            await Xamarin.Forms.Application.Current.MainPage.Navigation.PopAsync();
        }
        public string MiniRoundTimer
        {
            set
            {
                if (miniRoundTimer != value)
                {
                    miniRoundTimer = value;

                    OnPropertyChanged(nameof(MiniRoundTimer));
                }
            }
            get
            {
                return miniGame.TimerText;
            }
        }

        public string BookshelfHighlight
        {
            get
            {
                return bookshelfHighlight;
            }
            set
            {
                bookshelfHighlight = value;
                OnPropertyChanged(nameof(BookshelfHighlight));
            }
        }
        public string BinHighlight
        {
            get
            {
                return binHighlight;
            }
            set
            {
                binHighlight = value;
                OnPropertyChanged(nameof(BinHighlight));
            }
        }
        public string BasketHighlight
        {
            get
            {
                return basketHighlight;
            }
            set
            {
                basketHighlight = value;
                OnPropertyChanged(nameof(BasketHighlight));
            }
        }
        public bool InstructionsVisible
        {
            get
            {
                return instructionsVisible;
            }
            set
            {
                instructionsVisible = value;
                OnPropertyChanged(nameof(InstructionsVisible));
            }
        }
        public bool NotificationPopupVisible
        {
            get
            {
                return notificationPopupVisible;
            }
            set
            {
                notificationPopupVisible = value;
                OnPropertyChanged(nameof(NotificationPopupVisible));
            }
        }
        public bool SideBarVisible
        {
            get
            {
                return sideBarVisible;
            }
            set
            {
                sideBarVisible = value;
                OnPropertyChanged(nameof(SideBarVisible));
            }
        }
        public string NotificationPopupTitle
        {
            get
            {
                return notificationPopupTitle;
            }
            set
            {
                notificationPopupTitle = value;
                OnPropertyChanged(nameof(NotificationPopupTitle));
            }
        }
        public string NotificationPopupText
        {
            get
            {
                return notificationPopupText;
            }
            set
            {
                notificationPopupText = value;
                OnPropertyChanged(nameof(NotificationPopupText));
            }
        }

        public bool Watermelon1Visible
        {
            get
            {
                return watermelon1Visible;
            }
            set
            {
                watermelon1Visible = value;
                OnPropertyChanged(nameof(Watermelon1Visible));
            }
        }
        public bool Watermelon2Visible
        {
            get
            {
                return watermelon2Visible;
            }
            set
            {
                watermelon2Visible = value;
                OnPropertyChanged(nameof(Watermelon2Visible));
            }
        }
        public bool Watermelon3Visible
        {
            get
            {
                return watermelon3Visible;
            }
            set
            {
                watermelon3Visible = value;
                OnPropertyChanged(nameof(Watermelon3Visible));
            }
        }
        public bool Watermelon4Visible
        {
            get
            {
                return watermelon4Visible;
            }
            set
            {
                watermelon4Visible = value;
                OnPropertyChanged(nameof(Watermelon4Visible));
            }
        }
        public bool Laundry5Visible
        {
            get
            {
                return laundry5Visible;
            }
            set
            {
                laundry5Visible = value;
                OnPropertyChanged(nameof(Laundry5Visible));
            }
        }
        public bool Laundry6Visible
        {
            get
            {
                return laundry6Visible;
            }
            set
            {
                laundry6Visible = value;
                OnPropertyChanged(nameof(Laundry6Visible));
            }
        }
        public bool Laundry7Visible
        {
            get
            {
                return laundry7Visible;
            }
            set
            {
                laundry7Visible = value;
                OnPropertyChanged(nameof(Laundry7Visible));
            }
        }
        public bool Book8Visible
        {
            get
            {
                return book8Visible;
            }
            set
            {
                book8Visible = value;
                OnPropertyChanged(nameof(Book8Visible));
            }
        }
        public bool Book9Visible
        {
            get
            {
                return book9Visible;
            }
            set
            {
                book9Visible = value;
                OnPropertyChanged(nameof(Book9Visible));
            }
        }
        public bool Book10Visible
        {
            get
            {
                return book10Visible;
            }
            set
            {
                book10Visible = value;
                OnPropertyChanged(nameof(Book10Visible));
            }
        }
        void OnSideBarSelect()
        {

            SideBarVisible = true;
        }
        void OnSideBarNotSelect()
        {

            SideBarVisible = false;
        }


        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
