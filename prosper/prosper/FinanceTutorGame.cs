﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace prosper
{
    public class FinanceTutorGame
    {
        public const int MaxNumQuestions = 20;
        public bool GameWon { get; set; } = false;
        private FinanceQuestion[] GameQuestions { get; set; }
        public FinanceQuestion CurrentQuestion { get; private set; }
        private Stack<FinanceQuestion> RemainingQuestions { get; set; }
        public int NumCorrectQuestions { get; set; } = 0;
        public int NumIncorrectQuestions { get; set; } = 0;
        public int NumQuestionsAnswered
        {
            get
            {
                return NumCorrectQuestions + NumIncorrectQuestions;
            }
        }
        public bool GameLost { get; set; } = false;
        public bool IsTimerCancel { get; set; } = false;
        public string TimerText { get; private set; }
        public FinanceTutorGame()
        {
            FinanceQuestion q1 = new FinanceQuestion("Suppose you have $100 in savings account earning 2 percent interest a year. After 5 years, how much would you have?", 
                new string[] {"More than $102", "Exactly $102", "Less than $102"},
                "You’ll have more than $102 at the end of five years because your interest will compound over time. In other words, you earn interest on the money you save and on the interest your savings earned in prior years.");
            FinanceQuestion q2 = new FinanceQuestion("Imagine that the interest rate on your savings account is 1 percent a year and inflation is 2 percent a year. After one year, would the money in the account buy more than it does today, exactly the same or less than today?",
                new string[] { "Less", "Same", "More"},
                "Inflation is the rate at which the price of goods and services rises. If the annual inflation rate is 2 percent but the savings account only earns 1 percent, the cost of goods and services is growing faster than the money in the savings account that year.");
            FinanceQuestion q3 = new FinanceQuestion("If interest rates rise, what will typically happened to bond prices? Rise, fall, stay the same, or is there no relationship?",
                new string[] { "Fall", "Rise", "Stay the Same", "No Relationship" }, 
                "With the increase of interest rates, the interests of new bonds higher than the old one, causing the worth of old bonds to decline.");
            FinanceQuestion q4 = new FinanceQuestion("A 15-year mortgage typically requires higher monthly payments than a 30-year mortgage but the total interest over the life of the loan will be less.",
                new string[] { "True", "False"}, 
                "Although compared with each month, 30-year mortgage paid less, it means you will pay for a longer time and spend more money on interest.");
            FinanceQuestion q5 = new FinanceQuestion("Buying a single company's stock usually provides a safer return than a stock mutual fund.",
                new string[] { "False", "True"}, 
                "It's best not to put all your eggs in one basket. A stock mutual fund spreads the risk.");
            FinanceQuestion q6 = new FinanceQuestion("Which is always larger, your gross income or your net income?",
                new string[] { "Gross", "Net"}, 
                "Gross income is the amount of salary or wages paid to the individual by an employer, before any deductions are taken. Net income is the residual amount of earnings after all deductions have been taken from gross pay, such as payroll taxes, garnishments, and retirement plan contributions.");
            FinanceQuestion q7 = new FinanceQuestion("The greatest running cost of a new car is:",
                new string[] { "Car depreciation", "Petrol and servicing", "Registration", "Insurance" }, 
                "While other expenses are continuous and ongoing, the loss in value of your car is actually the greatest cost associated with owning a car.");
            FinanceQuestion q8 = new FinanceQuestion("Zane is 20 and has a $50 monthly cap on his phone bill. However, he has been making a lot more calls than his plan allows and now has a $720 bill to pay. What should Zane do?",
                new string[] { "Reduce his phone use and pay off the debt ASAP", "Change providers and pay off debt with credit card"},
                "Zane might have to pay an early termination fee of over $400 as well as the cost of the new mobile contract. Getting a credit card will only land him in more debt. He should instead negotiate a payment plan with his phone company and pay off the debt as soon as possible.");
            FinanceQuestion q9 = new FinanceQuestion("Justine is 19 and earning about $300 a week working at the local pizza joint to support her uni lifestyle. She is paid weekly but notices that she hasn't been paid any super. Is Justine entitled to super?",
                new string[] { "Yes", "No" }, 
                "Justine is entitled to super because she earns more than $450 per month and is over 18. Justine should approach her boss about being paid super.");
            FinanceQuestion q10 = new FinanceQuestion("Lucy has started work and has a HELP debt of $25,000. She also has a credit card debt of $1,000. She wants to put aside some of her weekly pay to clear off her debts. Which should she pay off first?",
                new string[] { "Credit card debt", "HELP debt"},
                "Credit cards charge a high rate of interest so it's a good idea to pay off these debts as soon as possible. Lucy will be required to start repaying her HELP debt through the tax system when she reaches the compulsory repayment threshold.");
            FinanceQuestion q11 = new FinanceQuestion("Chloe and Grant are saving to buy a house and hope to have a deposit within the next 5 years. Where is the best place to grow their deposit?",
                new string[] { "Online savings account", "Managed shared fund"},
                "An investment in shares, whether directly or through a managed fund, needs a longer investment timeframe in order to ride out fluctuations in the market. An online savings account will pay them interest and protect their capital, giving them the flexibility to access their money as soon as they are ready to buy a home.");
            FinanceQuestion q12 = new FinanceQuestion("Sam bought clothes from an American website but when he received his credit card statement, he found an unexplained charge of US$80. Can Sam get his $80 back?",
                new string[] { "Yes", "No"},
                "Sam should immediately contact his credit card company, report the unauthorised transaction and ask for it to be reversed.");
            FinanceQuestion q13 = new FinanceQuestion("You're about to take out a home loan and your lender has told you that you need to pay mortgage insurance. Would this insurance protect you if you were unable to keep up with repayments?",
                new string[] { "No", "Yes"}, 
                "Mortgage insurance protects your lender against the risk that you will not repay your loan. The insurer will pay out your lender, but is then entitled to chase you for the money.");
            FinanceQuestion q14 = new FinanceQuestion("Adrian organises travel insurance for his trip to Hanoi. A few years ago he hurt his knee playing football and needed to have surgery. Does he have to tell his insurer about this?",
                new string[] { "Yes", "No"},
                "If Adrian hurts his knee while he is overseas, the insurer may not cover his claim because he did not disclose his pre-existing medical condition. To make sure you are properly insured when travelling, you must be honest about your medical history.");
            FinanceQuestion q15 = new FinanceQuestion("An Annual Fee is the fee charged by credit card companies when you don’t make your payment on time.",
                new string[] { "False", "True"}, 
                "Annual fee is a kind of service charge annually, whether you used your credit card or not, if you own one, you have to pay for this service annually.");
            FinanceQuestion q16 = new FinanceQuestion("If you are struggling with your debts, refinancing will get you back in control.",
                new string[] { "Not necessarily", "Yes"}, 
                "Refinancing is not always suitable and can be a short-term fix to a long-term problem. You may also be charged additional fees to refinance which could put you in even more debt.");
            FinanceQuestion q17 = new FinanceQuestion("A debit card uses someone else's money for your purchases which you can pay back at a later date.",
                new string[] { "False", "True"},
                "A debit card uses money you have. Whereas a credit card uses borrowed money which you have to pay back.");
            FinanceQuestion q18 = new FinanceQuestion("The good thing about a budget is that you only have to do it once.",
                new string[] { "False", "True"}, "Budgets must be reviewed on a regular basis especially when your circumstances change.");
            FinanceQuestion q19 = new FinanceQuestion("You see a home loan advertised at 5.5% per annum. Next to that figure you see a 'comparison rate' quoted at 5.85%. What does 'comparison rate' mean?",
                new string[] { "The real interest rate when most fees and charges are taken into account", "The average rate charged for similar loans currently being offered" },
                "The comparison rate includes interest plus most fees and charges on the loan being advertised. Credit providers must provide a comparison rate for advertised loans and credit products. It gives you a better idea of how much the loan will really cost you and allows you to compare products from different lenders.");
            FinanceQuestion q20 = new FinanceQuestion("Georgia recently applied for a personal loan and was refused because she had a bad credit listing on her credit file. The listing was from 3 years ago, can the bank refuse her loan?",
                new string[] { "Yes", "No"},
                "Banks have the right to decide who they lend money to and they will look at your credit file when deciding whether or not to lend you money. Bad debts remain on your credit file for 5 to 7 years and will affect your credit score.");
            GameQuestions = new FinanceQuestion[] { q1, q2, q3, q4, q5, q6, q7, q8, q9, q10,
                q11, q12, q13, q14, q15, q16, q17, q18, q19, q20 };
            RemainingQuestions = new Stack<FinanceQuestion>(ShuffleQuestions(GameQuestions));
        }

        public void RestartGame()
        {
            RemainingQuestions = new Stack<FinanceQuestion>(ShuffleQuestions(GameQuestions));
            GameLost = false;
            GameWon = false;
            NumCorrectQuestions = 0;
            NumIncorrectQuestions = 0;
            IsTimerCancel = false;
            StartTimer();

        }
        public FinanceQuestion[] ShuffleQuestions(FinanceQuestion[] initialQuestions)
        {
            FinanceQuestion[] shuffledQuestions = initialQuestions;
            System.Random random = new System.Random();
            for (int i = 0; i < shuffledQuestions.Length; i++)
            {
                int j = random.Next(i, shuffledQuestions.Length); // Don't select from the entire array on subsequent loops
                FinanceQuestion temp = shuffledQuestions[i];
                shuffledQuestions[i] = shuffledQuestions[j];
                shuffledQuestions[j] = temp;
            }
            return shuffledQuestions;
        }

        public void CheckGameStatus()
        {
            //check if all questions answered
            if (NumQuestionsAnswered >= MaxNumQuestions)
            {
                GameWon = true;
                IsTimerCancel = true;
            }
            else if(NumIncorrectQuestions >= 5)
            {
                GameLost = true;
                IsTimerCancel = true;
            }
        }

        public void GetNextQuestion()
        {
            FinanceQuestion nextQuestion;
            nextQuestion = RemainingQuestions.Pop();
            CurrentQuestion = nextQuestion;
        }
        

        public void StartTimer()
        {
            int mins = 1;
            int counter = 0;
            Device.StartTimer(new TimeSpan(0, 0, 1), () =>
            {
                if (IsTimerCancel)
                {
                    return false;
                }
                else
                {
                    Device.BeginInvokeOnMainThread(() =>
                    {
                        counter = counter - 1;
                        if (counter < 0)
                        {
                            counter = 59;
                            mins = mins - 1;
                            if (mins < 0)
                            {
                                mins = 0;
                                counter = 0;
                            }
                        }

                        TimerText = string.Format("{0:0}:{1:00}", mins, counter);
                    });
                    if (mins == 0 && counter == 0)
                    {
                        //what to do when the timer finishes
                        //this is if they run out of time, but haven't got too many questions incorrect
                        GameWon = true;
                        return false;
                    }
                    else
                    {
                        return true;
                    }
                }
            });

        }
    }
}
