﻿using System;
using System.Collections.Generic;
using System.Text;

namespace prosper
{
    public class RandomEvent
    {
        public RandomEvent()
        {
            Random rnd = new Random();
            int selection = rnd.Next(0, 9); //random number between 0 and 9
            //select event
            //money change (win/lose, amount)
            //happiness (win/lose, amount)
            EventText = eventsArray[selection, 0];
            MoneyOutcome = eventsArray[selection, 1];
            MoneyAmount = Convert.ToDouble(eventsArray[selection, 2]);
            HappinessOutcome = eventsArray[selection, 3];
            HappinessAmount = Convert.ToDouble(eventsArray[selection, 4]);
        }
        public string EventText { get; set; }
        public string HappinessOutcome { get; set; }
        public double HappinessAmount { get; set; }
        public string MoneyOutcome { get; set; }
        public double MoneyAmount { get; set; }

        string[,] eventsArray = new string[10, 5] { 
            { "You washed your neighbours car and they gave you a nice tip.", 
                "win", "50", 
                "lose", "0.1" 
            }, 
            { "You found some spare change behind the couch and your parents let you keep it!", 
                "win", "20", 
                "win", "0.1"  }, 
            { "You lost a library book and have to pay a $20 fine.", 
                "lose", "20", 
                "lose", "0.1"  },
            { "You splurged at the school canteen and enjoyed it.", 
                "lose", "50", 
                "win", "0.2"  }, 
            { "Happy birthday! You've been sent money by your relatives", 
                "win", "100", 
                "win", "0.2"  }, 
            { "Your brother comes to hangout with you. You invite him to have some desserts", 
                "lost", "50", 
                "win", "0.1"  },
            { "You lost your wallet on your way home. Unluckily, you lost $50", 
                "lost", "50", 
                "lose", "0.1"  }, 
            { "You find $20 in your coat as you put it in last winter", 
                "win", "20", 
                "lose", "0.1"  }, 
            { "Your hard work has earned a school award", 
                "win", "50", 
                "lost", "0.1"  }, 
            { "You got a lottery ticket and won the price", 
                "win", "50", 
                "lose", "0.1"  }};




    }
}
