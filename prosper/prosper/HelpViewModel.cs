﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;


namespace prosper
{
    class HelpViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        public ICommand DisplayText { get; private set; }
        public ICommand SideBarCommand { get; private set; }
        public ICommand SideBarDisappear { get; private set; }

        string helpDisplayText;
        string[] helpAllTexts;
        bool sideBarVisible;

        public HelpViewModel ()
		{
            helpAllTexts = new string[6];
            helpAllTexts[0] = "You can spend your money to go to the store to buy anything you want. " +
                "Each item has its price and value of Happiness. After purchase, " +
                "your Happiness value will increase and your money will decrease. " +
                "This is the most important way for you to get happiness.";

            helpAllTexts[1] = "Happiness is the key to achieving your level. You can get the benefit " +
                "by purchasing the product. When it reaches a certain value, you will reach the next level.";
            helpAllTexts[2] = "You can play some mini games and you can earn some money.";
            helpAllTexts[3] = "You can manage your money according to Investment options.";
            helpAllTexts[4] = "After you finish 3 levels, you will win the game.";
            helpAllTexts[5] = "In a certain period of time, you will have a chance to get some random " +
                "events. Some of them are good for you, and some will make you lose something.";

            HelpDisplayText = helpAllTexts[0];
            //OnPropertyChanged(nameof(HelpDisplayText));
            DisplayText = new Command<string>((key) =>
            {
                int index = int.Parse(key);
                HelpDisplayText = helpAllTexts[index];
                //OnPropertyChanged(nameof(HelpDisplayText));
            });
            SideBarCommand = new Command(OnSideBarSelect);
            SideBarDisappear = new Command(OnSideBarNotSelect);
        }

        void OnSideBarSelect()
        {

            SideBarVisible = true;
        }
        void OnSideBarNotSelect()
        {

            SideBarVisible = false;
        }

        public string HelpDisplayText
        {
            set
            {
                helpDisplayText = value;
                OnPropertyChanged(nameof(HelpDisplayText));
            }
            get
            {
                return helpDisplayText;
            }
        }

        public bool SideBarVisible
        {
            get
            {
                return sideBarVisible;
            }
            set
            {
                sideBarVisible = value;
                OnPropertyChanged(nameof(SideBarVisible));
            }
        }

        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}