﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace prosper
{
    public class CleanTheHouseGame
    {
        public bool GameWon { get; set; } = false;
        public bool GameLost { get; set; } = false;
        public int NumDirtyItems { get; set; }
        public bool IsTimerCancel { get; set; } = false;
        public string TimerText { get; private set; }
        public Tool SelectedTool { get; set; }

        public CleanTheHouseGame()
        {
            //set up number of dirty items
            //initialise some stuff
            NumDirtyItems = 10;

        }

        public void CheckGameStatus()
        {
            if (NumDirtyItems <= 0)
            {
                GameWon = true;
                IsTimerCancel = true;
            }
        }

        public enum Tool
        {
            Bookshelf,
            Basket,
            Bin,
        };

        public void StartTimer()
        {
            int mins = 1;
            int counter = 0;
            Device.StartTimer(new TimeSpan(0, 0, 1), () =>
            {
                if (IsTimerCancel)
                {
                    return false;
                }
                else
                {
                    Device.BeginInvokeOnMainThread(() =>
                    {
                        counter = counter - 1;
                        if (counter < 0)
                        {
                            counter = 59;
                            mins = mins - 1;
                            if (mins < 0)
                            {
                                mins = 0;
                                counter = 0;
                            }
                        }

                        TimerText = string.Format("{0:0}:{1:00}", mins, counter);
                    });
                    if (mins == 0 && counter == 0)
                    {
                        //what to do when the timer finishes
                        //this is if they lose the game
                        GameLost = true;
                        return false;
                    }
                    else
                    {
                        return true;
                    }
                }
            });

        }
    }
}
