﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using Xamarin.Forms;
using System.Windows.Input;
using System.Linq;
using Android.OS;
using Android.Content;

namespace prosper
{
    class FinanceTutorViewModel : INotifyPropertyChanged
    {
        public Game game = Game.Instance;
        string miniRoundTimer;
        bool notificationPopupVisible = false;
        string notificationPopupText;
        string notificationPopupTitle;
        string outcomeImage;
        bool instructionsVisible = true;
        public event PropertyChangedEventHandler PropertyChanged;
        public ICommand PlayButtonCommand { get; private set; }
        public ICommand MyRoomButtonCommand { get; private set; }
        public ICommand AnswerQuestionCommand { get; private set; }
        public ICommand NextQuestionCommand { get; private set; }
        
        FinanceTutorGame miniGame = new FinanceTutorGame();
        public FinanceTutorViewModel()
        {
            AnswerQuestionCommand = new Command<string>(OnAnswerQuestion);
            PlayButtonCommand = new Command(StartGame);
            MyRoomButtonCommand = new Command(OnMyRoom);
            NextQuestionCommand = new Command(OnShowQuestion);
        }

        void StartGame()
        {
            InstructionsVisible = false;
            NotificationPopupVisible = false;
            miniGame.RestartGame();
            OnShowQuestion();
            Device.StartTimer(TimeSpan.FromSeconds(1), () =>
            {
                OnPropertyChanged(nameof(MiniRoundTimer));
                miniGame.CheckGameStatus();
                if (miniGame.GameWon)
                {
                    int moneyEarned = miniGame.NumCorrectQuestions * 5;
                    NotificationPopupTitle = "Quiz Complete!";
                    NotificationPopupText = "Congratulations you correctly answered " + miniGame.NumCorrectQuestions.ToString() + " questions correctly."
                    + "\nYou have earned $" + moneyEarned+".";
                    NotificationPopupVisible = true;
                    game.MoneyTotal += moneyEarned;
                    game.GameTransactions.Add(new Transaction(moneyEarned, "Finance Tutor Job"));
                    miniGame.IsTimerCancel = true;
                    return false;
                }
                if (miniGame.GameLost)
                {
                    NotificationPopupTitle = "Game Over";
                    NotificationPopupText = "You got too many questions wrong. Make sure to read the explanations and try again." +
                    "\nYou have not earned any money.";
                    NotificationPopupVisible = true;
                    miniGame.IsTimerCancel = true;
                    return false;
                }
                return true;
            });
        }

        void OnAnswerQuestion(string answer)
        {
            //Application.Current.MainPage.DisplayAlert("quest.ans", answer, "OK");
            //check if answerID is the correct answer for the current question
            bool correctAnswer = (miniGame.CurrentQuestion.IsAnswerCorrect(answer));

            //if correct answer
            if (correctAnswer)
            {
                miniGame.NumCorrectQuestions++;
                //display a tick and the explanation
                OutcomeImage = "tick.png";
            }
            else
            {
                miniGame.NumIncorrectQuestions++;
                //display a cross and the explanation
                OutcomeImage = "cross.png";
            }

            //show the explanation screen
            MessagingCenter.Send<FinanceTutorViewModel, string>(this, "Display Explanation", miniGame.CurrentQuestion.Explanation);

        }

        void OnShowQuestion()
        {
            if (miniGame.NumQuestionsAnswered < 21)
            {
                miniGame.GetNextQuestion();
            }
            else
            {
                miniGame.GameWon = true;
            }
            //call method to display question
            //need to do something in UI layer
            MessagingCenter.Send<FinanceTutorViewModel, FinanceQuestion>(this, "Display Question", miniGame.CurrentQuestion);
        }

        async void OnMyRoom()
        {
            //take user back to my room
            Page currentPage = App.Current.MainPage.Navigation.NavigationStack.LastOrDefault();
            //Application.Current.MainPage.Navigation.InsertPageBefore(new SelectCharacter(), currentPage);
            await Xamarin.Forms.Application.Current.MainPage.Navigation.PopAsync();
        }
        public string MiniRoundTimer
        {
            set
            {
                if (miniRoundTimer != value)
                {
                    miniRoundTimer = value;

                    OnPropertyChanged(nameof(MiniRoundTimer));
                }
            }
            get
            {
                return miniGame.TimerText;
            }
        }
        public bool InstructionsVisible
        {
            get
            {
                return instructionsVisible;
            }
            set
            {
                instructionsVisible = value;
                OnPropertyChanged(nameof(InstructionsVisible));
            }
        }
        public bool NotificationPopupVisible
        {
            get
            {
                return notificationPopupVisible;
            }
            set
            {
                notificationPopupVisible = value;
                OnPropertyChanged(nameof(NotificationPopupVisible));
            }
        }
        public string NotificationPopupTitle
        {
            get
            {
                return notificationPopupTitle;
            }
            set
            {
                notificationPopupTitle = value;
                OnPropertyChanged(nameof(NotificationPopupTitle));
            }
        }
        public string NotificationPopupText
        {
            get
            {
                return notificationPopupText;
            }
            set
            {
                notificationPopupText = value;
                OnPropertyChanged(nameof(NotificationPopupText));
            }
        }

        public string OutcomeImage
        {
            get
            {
                return outcomeImage;
            }
            set
            {
                outcomeImage = value;
                OnPropertyChanged(nameof(OutcomeImage));
            }
        }


        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
